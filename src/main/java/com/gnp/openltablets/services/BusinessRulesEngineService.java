package com.gnp.openltablets.services;

import java.util.Date;

import org.openl.rules.runtime.RulesEngineFactory;
import org.openl.runtime.EngineFactory;
import org.springframework.stereotype.Service;

import com.gnp.openltablets.dto.PayloadDTO;

@Service
public class BusinessRulesEngineService implements IBusinessRulesEngineService {

	@Override
	public Object vigenciaSolicitud(Date fechaEmision, Date fechaFirma) {
		BusinessRules instance = getBusinessRules();
		Object vigencia = instance.vigenciaSolicitud(fechaEmision, fechaFirma);
		return vigencia;
	}

	@Override
	public String isEdadAceptacion(PayloadDTO payload) {
		BusinessRules instance = getBusinessRules();
		String semaforo = instance.edadAceptacion(payload.getCobertura(), payload.getEdad(), payload.getMoneda(),
				payload.getProducto());
		if (semaforo == null)
			return "FALSO (CONTROLADO)";
		return semaforo;
	}

	@Override
	public Double edadReal(Date fechaNacimiento, Date fechaFirma) {
		BusinessRules instance = getBusinessRules();
		Double edadReal = instance.yearsCount(fechaNacimiento, fechaFirma);
		return edadReal;
	}

	private BusinessRules getBusinessRules() {
		String xlsxPath = "/home/fernando/rules.xlsx";
		final EngineFactory<BusinessRules> engineFactory = new RulesEngineFactory<BusinessRules>(xlsxPath,
				BusinessRules.class);
		return engineFactory.newEngineInstance();
	}

	
	public interface BusinessRules {
		Object vigenciaSolicitud(Date fechaEmision, Date fechaFirma);

		String edadAceptacion(String cobertura, Integer edad, String moneda, String producto);

		Double yearsCount(Date fechaNacimiento, Date fechaFirma);
	}

}
