package com.gnp.openltablets.services;

import java.util.Date;

import com.gnp.openltablets.dto.PayloadDTO;

public interface IBusinessRulesEngineService {
	String isEdadAceptacion(PayloadDTO payload);
	Object vigenciaSolicitud(Date fechaEmision, Date fechaFirma);
	Double edadReal(Date fechaNacimiento, Date fechaFirma);
}
