package com.gnp.openltablets.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gnp.openltablets.dto.PayloadDTO;
import com.gnp.openltablets.services.BusinessRulesEngineService;

@RestController
public class RulesController {
	@Autowired
	private BusinessRulesEngineService rulesService;
	
	@GetMapping("/rules/vigenciaSolicitud")
	public Object vigenciaSolicitud(@RequestBody PayloadDTO req) throws ParseException {
		DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaEmision = sourceFormat.parse(req.getFechaEmision());
		Date fechaFirma = sourceFormat.parse(req.getFechaFirma());
		return rulesService.vigenciaSolicitud(fechaEmision, fechaFirma);
	}

	@GetMapping("/rules/edadAceptacion")
	public Map<String, String> isEdadAceptacion(@RequestBody PayloadDTO req) {
		return Collections.singletonMap("edadAceptacion", rulesService.isEdadAceptacion(req));
	}
	
	@GetMapping("/rules/edadReal")
	public Double edadReal(@RequestBody PayloadDTO req) throws ParseException {
		DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaNacimiento = sourceFormat.parse(req.getFechaNacimiento());
		Date fechaFirma = sourceFormat.parse(req.getFechaFirma());
		return rulesService.edadReal(fechaNacimiento, fechaFirma);
	}
	
}
