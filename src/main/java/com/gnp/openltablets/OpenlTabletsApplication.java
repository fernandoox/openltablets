package com.gnp.openltablets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenlTabletsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenlTabletsApplication.class, args);
	}

}
